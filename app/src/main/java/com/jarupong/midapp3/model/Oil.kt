package com.jarupong.midapp3.model

data class Oil(val name: String, val price: Double)